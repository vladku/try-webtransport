# try-webtransport

Implemented some simple ping/pong

## To run

1. Generate `openssl req -x509 -newkey rsa:4096 -nodes -keyout server.key -out server.crt -days 365 -subj '/CN=localhost'`
1. Run server use `python server.py`
1. Run client use `python server.py`