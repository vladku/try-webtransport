import asyncio
import ssl
import time
from aioquic.asyncio import connect
from aioquic.quic.configuration import QuicConfiguration
from aioquic.asyncio.protocol import QuicConnectionProtocol
from aioquic.h3.connection import H3_ALPN, H3Connection
from aioquic.h3.events import H3Event, HeadersReceived, DataReceived
from aioquic.quic.events import StreamDataReceived
from aioquic.quic.connection import QuicConnection

from server import WebTransportProtocol

class WebTransportProtocolClient(WebTransportProtocol):
# class WebTransportProtocolClient(QuicConnectionProtocol):
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.h3 = H3Connection(self._quic)
    #     self.stream_id = None

    # def quic_event_received(self, event: object) -> None:
    #     # print(f"Receive {event}")
    #     if isinstance(event, StreamDataReceived):
    #         data = event.data#.decode()
    #         print(f"Received data: {data}")
    #     elif isinstance(event, HeadersReceived) and event.stream_id == self.stream_id:
    #         headers = dict(event.headers)
    #         print(f"Received headers: {headers}")
    #     elif isinstance(event, DataReceived) and event.stream_id == self.stream_id:
    #         data = event.data#.decode()
    #         print(f"Received data: {data}")
    #     elif isinstance(event, HeadersReceived) and event.stream_id == 0:
    #         headers = dict(event.headers)
    #         if headers.get(":status") == "200":
    #             self.stream_id = event.stream_id
    #             print(f"Connected with stream ID: {self.stream_id}")
    #     # elif isinstance(event, ConnectionTerminated):
    #     #     print(f"Connection terminated: {event}")

    async def handle_webtransport(self):
        # stream = self.h3.create_stream()
        headers = [
            (b":method", b"CONNECT"),
            (b":protocol", b"webtransport"),
            (b":scheme", b"https"),
            (b":authority", b"test"),
            (b":path", b"/echo"),
            (b"user-agent", b"aioquic"),
        ]
        # self.stream_id = await self.h3.create_stream()
        stream_id = self._quic.get_next_available_stream_id()
        self._http.send_headers(stream_id, headers=headers, end_stream=False)
        # while self.stream_id is None:
        #     await asyncio.sleep(0.1)
        # data = "Ping".encode()
        # self._http.send_data(self.stream_id, data, end_stream=False)
        response_id = self._http.create_webtransport_stream(
                        stream_id, is_unidirectional=False)
        self.response_id = response_id
        self._http._quic.send_stream_data(
                    response_id, b"Ping", end_stream=True)
        # data = "Hello, world!".encode()
        # self.h3.send_datagram(self.stream_id, data)
        await asyncio.sleep(1)


async def main():
    config = QuicConfiguration(
        alpn_protocols=H3_ALPN,
        is_client=True,
        max_datagram_frame_size=65536,
    )
    config.load_verify_locations(cafile="server.crt")

    async with connect(
        "0.0.0.0",
        4433,
        configuration=config,
        create_protocol=WebTransportProtocolClient,
    ) as client:
        await client.handle_webtransport()
        await asyncio.sleep(1)

def run():
    asyncio.run(main())

if __name__ == "__main__":
    run()